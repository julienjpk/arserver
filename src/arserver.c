/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

#ifdef HAVE_ERRNO_H
#include <errno.h>
#endif

#include "airspace.h"
#include "feedback.h"
#include "operations.h"

int main(int argc, char* argv[])
{
    if(argc < 3){
	fprintf(stderr, "Usage: %s [interface] [port].\n", argv[0]);
	return EXIT_FAILURE;
    }

    int s, port;
    
    airspace_t* airspace;
    feedback_server_t* feedback;
    op_server_t* operations;
    
    pthread_t airspace_thd, feedback_thd, operations_thd;

    /* Airspace initialisation:
     * This first thread is expected to crawl the network for drones, and 
     * register them as they are found. Drone detection is achieved by 
     * attempting to reach TCP port 5559 (control channel) on each host. */
    if(!(airspace = airspace_init(argv[1]))){
	fprintf(stderr, "%s: unable to setup airspace surveillance on %s.\n",
		argv[0], argv[1]);
	return EXIT_FAILURE;
    }

    if((s = pthread_create(&airspace_thd, NULL,
			   airspace_worker, airspace)) < 0){
	fprintf(stderr, "%s: unable to start airspace on %s: %s.\n",
		argv[0], argv[1], strerror(s));
	return EXIT_FAILURE;
    }

    /* Feedback initialisaion:
     * This second thread is responsible for every data stream coming from the 
     * drones and usually destined to "clients". The master server is expected 
     * to collect all that data, and distribute it to the clients as they 
     * request it. */
    if(!(feedback = feedback_init(airspace))){
	fprintf(stderr, "%s: unable to setup feedback.\n", argv[0]);
	return EXIT_FAILURE;
    }

    if((s = pthread_create(&feedback_thd, NULL, 
			   feedback_worker, feedback)) < 0){
	fprintf(stderr, "%s: unable to start feedback collection: %s.\n",
		argv[0], strerror(s));
	return EXIT_FAILURE;
    }

    /* Operations initialisation:
     * This part is responsible for clients management. */
    port = (int)strtol(argv[2], NULL, 10);
    if(!(operations = op_server_init(airspace, port))){
	fprintf(stderr, "%s: unable to setup the operations server on port "
		"%d.\n", argv[0], port);
	return EXIT_FAILURE;
    }

    if((s = pthread_create(&operations_thd, NULL,
			   op_server_worker, operations)) < 0){
	fprintf(stderr, "%s: unable to start the operations server.\n",
		argv[0]);
	return EXIT_FAILURE;
    }

    pthread_join(operations_thd, NULL);
    pthread_join(feedback_thd, NULL);
    pthread_join(airspace_thd, NULL);
    op_server_free(operations);
    feedback_free(feedback);
    airspace_free(airspace);

    return EXIT_SUCCESS;
}
