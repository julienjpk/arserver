/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _ARSERVER_COMMAND_H
#define _ARSERVER_COMMAND_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_TIME_H
#include <time.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "constants.h"
#include "util.h"

/* Early definitions */
typedef struct drone drone_t;
typedef struct group group_t;
typedef struct target target_t;
typedef struct op_client op_client_t;
typedef struct op_command_handler op_command_handler_t;
typedef struct op_server op_server_t;

/* One command. */
typedef struct op_command{
    op_command_handler_t* handler;
    char* args;
    unsigned char ack;
    struct timeval expiration;
    op_client_t* client;
} op_command_t;

/* The command server (thread). */
typedef struct op_comm_process{
    op_server_t* server;
    
    pthread_mutex_t signal_mtx;
    pthread_cond_t signal;
    queue_t* commands;
} op_comm_process_t;

/* A command handler (see command.c). */
typedef struct op_command_handler {
    const char* identifier;
    comm_type_t type;
    float duration;
    comm_status_t (*handler)(op_command_t*, op_comm_process_t*);
} op_command_handler_t;

/* Prepares a command processing thread. */
op_comm_process_t* op_comm_init(op_server_t* server);
/* Frees op_comm_process_t. */
void op_comm_free(op_comm_process_t* comm);
/* Get a target (drone or group) from airspace. */
comm_status_t op_comm_get_target(op_comm_process_t* process, op_command_t* comm,
				 char* id_str, unsigned char need_ownership,
				 target_t* target);
/* Get a drone from airspace. */
comm_status_t op_comm_get_drone(op_comm_process_t* process, op_command_t* comm,
				char* id_str, unsigned char need_ownership,
				drone_t** drone);
/* Get a group from airspace. */
comm_status_t op_comm_get_group(op_comm_process_t* process, op_command_t* comm,
				char* id_str, unsigned char need_ownership,
				group_t** group);
/* Command worker (thread routine). */
void* op_comm_worker(void* raw_comm);

/* Adds a command to the command queue. */
op_command_t* op_command_queue(op_comm_process_t* comm,
			       op_client_t* client,
			       char* line);
/* Frees a command. */
void op_command_free(void* raw_command);

/** HANDLERS: these functions are called for specific commands. **/

comm_status_t op_comm_acquire(op_command_t* command,
			      op_comm_process_t* process);
comm_status_t op_comm_release(op_command_t* command,
			      op_comm_process_t* process);
comm_status_t op_comm_airspace(op_command_t* command,
			       op_comm_process_t* process);

comm_status_t op_comm_newgroup(op_command_t* command,
			       op_comm_process_t* process);
comm_status_t op_comm_delgroup(op_command_t* command,
			       op_comm_process_t* process);
comm_status_t op_comm_attach(op_command_t* command,
			     op_comm_process_t* process);
comm_status_t op_comm_detach(op_command_t* command,
			     op_comm_process_t* process);

comm_status_t op_comm_basicctl(op_command_t* command,
			       op_comm_process_t* process);
comm_status_t op_comm_emergency(op_command_t* command,
				op_comm_process_t* process);

comm_status_t op_comm_pilot1(op_command_t* command,
			     op_comm_process_t* process);
comm_status_t op_comm_pilot5(op_command_t* command,
			     op_comm_process_t* process);
comm_status_t op_comm_status(op_command_t* command,
			     op_comm_process_t* process);

#endif
