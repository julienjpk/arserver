/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#include "command.h"
#include "operations.h"

/* Available commands. This structure is used to statistically link commands to 
 * their identifiers and handlers. The first integer (boolean) should be 1 when 
 * the command is sequenced, 0 otherwise. */
op_command_handler_t command_handlers[] = {
    { "ACQUIRE", COMM_ACQUIRE, 0, op_comm_acquire },
    { "RELEASE", COMM_RELEASE, 0, op_comm_release },
    { "AIRSPACE", COMM_AIRSPACE, 0, op_comm_airspace },

    { "NEWGROUP", COMM_NEWGROUP, 0, op_comm_newgroup },
    { "DELGROUP", COMM_DELGROUP, 0, op_comm_delgroup },
    { "ATTACH", COMM_ATTACH, 0, op_comm_attach },
    { "DETACH", COMM_DETACH, 0, op_comm_detach },
    
    { "TAKEOFF", COMM_TAKEOFF, 0, op_comm_basicctl },
    { "LANDING", COMM_LANDING, 0, op_comm_basicctl },
    { "EMERGENCY", COMM_EMERGENCY, 0, op_comm_emergency },
    
    { "ROLL", COMM_ROLL, 1.5, op_comm_pilot1 },
    { "PITCH", COMM_PITCH, 1.5, op_comm_pilot1 },
    { "THROTTLE", COMM_THROTTLE, 1.5, op_comm_pilot1 },
    { "YAW", COMM_YAW, 1.5, op_comm_pilot1 },
    { "PILOT", COMM_PILOT, 1.5, op_comm_pilot5 },

    { "STATUS", COMM_STATUS, 0, op_comm_status },
    
    { NULL, 0, 0, NULL }
};

op_comm_process_t* op_comm_init(op_server_t* server){
    if(!server) return NULL;
    
    op_comm_process_t* comm = malloc(sizeof(op_comm_process_t));
    comm->server = server;
    comm->commands = queue_new();

    pthread_mutex_init(&(comm->signal_mtx), NULL);
    pthread_mutex_lock(&(comm->signal_mtx));
    pthread_cond_init(&(comm->signal), NULL);
    return comm;
}

void op_comm_free(op_comm_process_t* comm){
    if(!comm) return;
    queue_free(comm->commands, op_command_free);
    free(comm);
}

comm_status_t op_comm_get_target(op_comm_process_t* process, op_command_t* comm,
				 char* id_str, unsigned char need_ownership,
				 target_t* target){
    char* star = strchr(id_str, '*');
    
    if(!star){
	drone_t* drone = NULL;
	comm_status_t s = op_comm_get_drone(process, comm, id_str,
					    need_ownership, &drone);
	if(s != ERR_NONE) return s;

	target->type = DRONE;
	target->target = drone;
	return ERR_NONE;
    }

    *star = 0;
    group_t* group = NULL;
    comm_status_t s = op_comm_get_group(process, comm, id_str, need_ownership,
					&group);
    if(s != ERR_NONE) return s;

    target->type = GROUP;
    target->target = group;
    return ERR_NONE;
}

comm_status_t op_comm_get_drone(op_comm_process_t* process, op_command_t* comm,
				char* id_str, unsigned char need_ownership,
				drone_t** drone){
    if(!process || !comm || !id_str) return ERR_ARGS;

    int drone_id = (int)strtol(id_str, NULL, 0);
    queue_t* drones = process->server->air->drones;
    drone_t* drone_cur = NULL;
    *drone = NULL;

    pthread_mutex_lock(&(drones->lock));
    iterator_t it = queue_iterator(drones);
    for(; !drone_cur && IT_HASNEXT(it); IT_NEXT(it)){
	drone_cur = IT_CUR(it, drone_t);
	if(drone_cur->id != drone_id) drone_cur = NULL;
    }

    if(!drone_cur){
	pthread_mutex_unlock(&(drones->lock));
	return ERR_NODRONE;
    }

    if(need_ownership && (!drone_cur->handler ||
			  drone_cur->handler != comm->client)){
	pthread_mutex_unlock(&(drones->lock)); 
	return ERR_DENIED;
    }
    
    *drone = drone_cur;

    pthread_mutex_unlock(&(drones->lock));
    return ERR_NONE;
}

comm_status_t op_comm_get_group(op_comm_process_t* process, op_command_t* comm,
				char* id_str, unsigned char need_ownership,
				group_t** group){
    if(!process || !comm || !id_str) return ERR_ARGS;

    int group_id = (int)strtol(id_str, NULL, 0);
    group_t* group_i = group_get(process->server->air, group_id);
    if(!group_i) return ERR_NODRONE;

    if(need_ownership && group_i->handler != comm->client)
	return ERR_DENIED;
    
    *group = group_i;
    return ERR_NONE;
}

void* op_comm_worker(void* raw_comm){
    op_comm_process_t* comm = (op_comm_process_t*)raw_comm;
    if(!comm) pthread_exit(NULL);

    op_command_t* command;
    comm_status_t status;
    iterator_t it;
    struct timeval now, start, end;
    char *out = NULL;

    while(comm->server->active){
	while(comm->commands->len > 0){
	    pthread_mutex_lock(&(comm->commands->lock));
	    it = queue_iterator(comm->commands);

	    gettimeofday(&start, NULL);
	    
	    for(; IT_HASNEXT(it); IT_NEXT(it)){
		command = IT_CUR(it, op_command_t);
		gettimeofday(&now, NULL);

		log_entry(LOG_COMMAND, "Having a look at %s, %ld:%ld to "
			  "%ld:%ld", command->handler->identifier, now.tv_sec,
			  now.tv_usec, command->expiration.tv_sec,
			  command->expiration.tv_usec);

		if(command->ack && (
		       now.tv_sec >= command->expiration.tv_sec ||
		       (now.tv_sec == command->expiration.tv_sec &&
			now.tv_usec >= command->expiration.tv_usec))){
		    log_entry(LOG_COMMAND, "Removing %s",
			      command->handler->identifier);
		    queue_remove(&it, op_command_free);
		    continue;
		}

		log_entry(LOG_COMMAND, "Handling %s for %s",
			  command->handler->identifier,
			  inet_ntoa(command->client->addr.sin_addr));
		
		status = command->handler->handler(command, comm);

		if(!command->ack && status != ERR_NONE){
		    op_client_send(command->client, "- %s %d",
				   command->handler->identifier, status);
		    queue_remove(&it, op_command_free);
		}
		
		command->ack = 1;
	    }

	    pthread_mutex_unlock(&(comm->commands->lock));

	    gettimeofday(&end, NULL);
	    end.tv_sec -= start.tv_sec;
	    end.tv_usec -= start.tv_usec;
	    
	    usleep(100000 / 3 - (1000000 * end.tv_sec + end.tv_usec));
	}

	pthread_cond_wait(&(comm->signal), &(comm->signal_mtx));
    }

    pthread_exit(NULL);
}

op_command_t* op_command_queue(op_comm_process_t* comm,
			       op_client_t* client,
			       char* line){
    char *out = NULL, *err_key = NULL;
    comm_status_t err = 0;

    if(!client || !line || strlen(line) <= 1){
	err_key = line;
	err = ERR_SYNTAX;
	goto error_report;
    }

    char* opening = strchr(line, '(');
    char* ending = strrchr(line, ')');
    char* id = line;
    char* args = NULL;
    
    if(opening && ending && (ending - opening) > 1){
	args = opening + 1;
	*(opening - 1) = 0;
	*ending = 0;
    }

    struct op_command_handler *h;
    for(h = command_handlers;
	h->handler && strcmp(h->identifier, id) != 0;
	h++);
    
    if(!h->handler){
	err_key = id;
	err = ERR_UNKNOWN;
	goto error_report;
    }

    op_command_t* command = malloc(sizeof(op_command_t));
    command->handler = h;
    command->args = args ? strdup(args) : NULL;
    command->ack = 0;

    if(h->duration <= 0){
	command->expiration.tv_sec = 0;
	command->expiration.tv_usec = 0;
    }
    else{
	gettimeofday(&(command->expiration), NULL);
	command->expiration.tv_sec += (int)(h->duration);
	command->expiration.tv_usec +=
	    1000000 * (h->duration - (int)(h->duration));
    }
    
    command->client = client;
    queue_add(comm->commands, command);

    pthread_mutex_lock(&(comm->signal_mtx));
    pthread_cond_signal(&(comm->signal));
    pthread_mutex_unlock(&(comm->signal_mtx));
    
    return command;

error_report:
    op_client_send(client, "- %s %d", err_key, err);
    return NULL;
}

void op_command_free(void* raw_command){
    op_command_t* comm = (op_command_t*)raw_command;
    if(comm->args) free(comm->args);
    free(comm);
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: ACQUIRE
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_acquire(op_command_t* command,
			      op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;

    int drone_id = (int)strtol(command->args, NULL, 0);
    queue_t* drones = process->server->air->drones;
    drone_t* drone = NULL;

    log_entry(LOG_COMMAND, "Acquisition request by %s (drone %d)",
	      inet_ntoa(command->client->addr.sin_addr), drone_id);

    pthread_mutex_lock(&(drones->lock));
    iterator_t it = queue_iterator(drones);
    for(; !drone && IT_HASNEXT(it); IT_NEXT(it)){
	drone = IT_CUR(it, drone_t);
	if(drone->id != drone_id) drone = NULL;
    }

    if(!drone){
	pthread_mutex_unlock(&(drones->lock)); 
	return ERR_NODRONE;
    }

    if(drone->handler/* && drone->handler != command->client*/){
	pthread_mutex_unlock(&(drones->lock)); 
	return ERR_BUSY;
    }

    drone->handler = command->client;
    op_client_send(command->client, "+ ACQUIRE (%d)", drone_id);
    pthread_mutex_unlock(&(drones->lock)); 
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: RELEASE
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_release(op_command_t* command,
			      op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;

    queue_t* drones = process->server->air->drones;
    drone_t* drone = NULL;
    comm_status_t s = op_comm_get_drone(process, command, command->args, 1,
					&drone);

    if(s != ERR_NONE) return s;
    if(drone->group_refs > 0) return ERR_DENIED;

    log_entry(LOG_COMMAND, "Release by %s (drone %d)",
	      inet_ntoa(command->client->addr.sin_addr), drone->id);
    
    pthread_mutex_lock(&(drones->lock)); 
    drone->handler = NULL;
    op_client_send(command->client, "+ RELEASE (%d)", drone->id);
    pthread_mutex_unlock(&(drones->lock)); 
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: AIRSPACE
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_airspace(op_command_t* command,
			       op_comm_process_t* process){
    queue_t* drones = process->server->air->drones;
    drone_t* drone = NULL;
    
    log_entry(LOG_COMMAND, "Airspace description request from %s",
	      inet_ntoa(command->client->addr.sin_addr));

    pthread_mutex_lock(&(drones->lock));
    
    char* desc = malloc(12 + drones->len * 24);    
    iterator_t it = queue_iterator(drones);
    for(; IT_HASNEXT(it); IT_NEXT(it)){
	drone = IT_CUR(it, drone_t);
	sprintf(desc, "%s%d:\"%s\",", desc, drone->id,
		inet_ntoa(drone->addr.sin_addr));
    }
    
    pthread_mutex_unlock(&(drones->lock));

    char* comma = strrchr(desc, ',');
    if(comma) *comma = 0;

    op_client_send(command->client, "+ AIRSPACE (%s)", desc);
    free(desc);
    
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: GROUP CREATION
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_newgroup(op_command_t* command,
			       op_comm_process_t* process){
    queue_t* groups = process->server->air->groups;
    group_t* newgroup = group_new(process->server->air, command->client);
    
    log_entry(LOG_COMMAND, "New group request from %s (group %d).",
	      inet_ntoa(command->client->addr.sin_addr), newgroup->id);

    queue_add(groups, newgroup);
    op_client_send(command->client, "+ NEWGROUP (%d)", newgroup->id);
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: GROUP DELETION
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_delgroup(op_command_t* command,
			       op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;
    
    group_t* group = NULL;
    comm_status_t s = op_comm_get_group(process, command, command->args, 1,
					&group);
    if(s != ERR_NONE) return s;
    
    log_entry(LOG_COMMAND, "Group deletion request from %s (group %d)",
	      inet_ntoa(command->client->addr.sin_addr), group->id);

    group_del(process->server->air, group->id);
    op_client_send(command->client, "+ DELGROUP (%s)", command->args);
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: DRONE GROUPING
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_attach(op_command_t* command,
			     op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;
    
    char* comma = strchr(command->args, ',');
    if(!comma || *comma == 0) return ERR_ARGS;
    *comma = 0;
    
    char *drone_id = command->args, *group_id = comma + 1;
    
    drone_t* drone = NULL;
    comm_status_t s = op_comm_get_drone(process, command, drone_id, 1, &drone);
    if(s != ERR_NONE) return s;

    group_t* group = NULL;
    s = op_comm_get_group(process, command, group_id, 1, &group);
    if(s != ERR_NONE) return s;

    iterator_t it = queue_iterator(group->drones);
    for(; IT_HASNEXT(it); IT_NEXT(it))
	if(IT_CUR(it, drone_t)->id == drone->id)
	    goto confirm;
    
    log_entry(LOG_COMMAND, "Grouping request from %s: drone %d, group %d.",
	      inet_ntoa(command->client->addr.sin_addr), drone->id, group->id);

    queue_add(group->drones, drone);
    (drone->group_refs)++;

confirm:
    op_client_send(command->client, "+ ATTACH (%d)", drone->id);
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: DRONE UNGROUPING
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_detach(op_command_t* command,
			     op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;
    
    char* comma = strchr(command->args, ',');
    if(!comma || *comma == 0) return ERR_ARGS;
    *comma = 0;
    
    char *drone_id = command->args, *group_id = comma + 1;
    
    drone_t* drone = NULL;
    comm_status_t s = op_comm_get_drone(process, command, drone_id, 1, &drone);
    if(s != ERR_NONE) return s;

    group_t* group = NULL;
    s = op_comm_get_group(process, command, group_id, 1, &group);
    if(s != ERR_NONE) return s;

    iterator_t it = queue_iterator(group->drones);
    for(; IT_HASNEXT(it); IT_NEXT(it)){
	if(IT_CUR(it, drone_t)->id == drone->id){
	    (drone->group_refs)--;
	    
	    log_entry(LOG_COMMAND, "Ungrouping request from %s: drone %d, "
		      "group %d.", inet_ntoa(command->client->addr.sin_addr),
		      drone->id, group->id);

	    queue_remove(&it, NULL);
	    goto confirm;
	}
    }

confirm:
    op_client_send(command->client, "+ DETACH (%d)", drone->id);
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: BASIC CONTROL
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_basicctl(op_command_t* command,
			       op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;

    queue_t* drones = process->server->air->drones;
    comm_type_t ctype = command->handler->type;
    target_t target;    
    comm_status_t s = op_comm_get_target(process, command, command->args, 1,
					 &target);

    if(s != ERR_NONE) return s;

    int32_t code = 0;
    code |= (1 << 18) | (1 << 20) | (1 << 22) | (1 << 24) | (1 << 28);
    if(ctype == COMM_TAKEOFF) code |= (1 << 9);

    pthread_mutex_lock(&(drones->lock));
    drone_target_send(&target, "REF", "%d", code);
    pthread_mutex_unlock(&(drones->lock));

    if(!command->ack){
	log_entry(LOG_COMMAND, "%s request by %s (target %s)",
		  (ctype == COMM_TAKEOFF ? "Takeoff" : "Landing"),
		  inet_ntoa(command->client->addr.sin_addr), command->args);

	op_client_send(command->client, "+ %s (%s)",
		       command->handler->identifier,
		       command->args);
    }
    
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: EMERGENCY
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_emergency(op_command_t* command,
				op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;

    queue_t* drones = process->server->air->drones;
    target_t target;
    comm_status_t s = op_comm_get_target(process, command, command->args, 1,
					 &target);

    if(s != ERR_NONE) return s;
    if(target.type != DRONE) return ERR_DENIED;

    drone_t* drone = (drone_t*)(target.target);
    
    pthread_mutex_lock(&(drones->lock));

    log_entry(LOG_COMMAND, "Emergency failsafe %s by %s",
	      (drone->emergency ? "confirmed" : "requested"),
	      inet_ntoa(command->client->addr.sin_addr));
    
    if(!drone->emergency){
	drone->emergency = 1;
	pthread_mutex_unlock(&(drones->lock));
	return ERR_REPEAT;
    }
	
    drone_target_send(&target, "REF", "%d", 1 << 8);
    op_client_send(command->client, "+ EMERGENCY (%d)", drone->id);
    drone->emergency = 0;

    pthread_mutex_unlock(&(drones->lock));
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: BASIC PILOTING
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_pilot1(op_command_t* command,
			     op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;

    char* comma = strchr(command->args, ',');
    if(!comma || *comma == 0) return ERR_ARGS;    
    char* value = comma + 1;
    *comma = 0;

    float alteration_f = strtof(value, NULL);
    int alteration = *((int*)&alteration_f);
    *comma = ',';

    if(alteration_f < -1.0 || alteration_f > 1.0)
	return ERR_ARGS;

    queue_t* drones = process->server->air->drones;
    target_t target;
    comm_status_t s = op_comm_get_target(process, command, command->args, 0,
					 &target);

    if(s != ERR_NONE) return s;

    pthread_mutex_lock(&(drones->lock));
    switch(command->handler->type){
	case COMM_ROLL:
	    drone_target_send(&target, "PCMD", "%d,%d,%d,%d,%d", 1, alteration,
			      0, 0, 0);
	    break;
	case COMM_PITCH:
	    drone_target_send(&target, "PCMD", "%d,%d,%d,%d,%d", 1, 0,
			      alteration, 0, 0);
	    break;
	case COMM_THROTTLE:
	    drone_target_send(&target, "PCMD", "%d,%d,%d,%d,%d", 1, 0, 0,
			      alteration, 0);
	    break;
	case COMM_YAW:
	    drone_target_send(&target, "PCMD", "%d,%d,%d,%d,%d", 1, 0, 0, 0,
			      alteration);
	    break;
    }
    
    if(!command->ack){
	log_entry(LOG_COMMAND, "Pilot command (%s, adjust by %.2f) from %s",
		  command->handler->identifier, alteration_f,
		  inet_ntoa(command->client->addr.sin_addr));
	
	op_client_send(command->client, "+ %s (%s)",
		       command->handler->identifier, command->args);
    }

    pthread_mutex_unlock(&(drones->lock)); 
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: ADVANCED PILOTING
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_pilot5(op_command_t* command,
			     op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;

    char* strp = NULL;
    char* drone_id = strtok_r(command->args, ",", &strp);
    char* roll = strtok_r(NULL, ",", &strp);
    char* pitch = strtok_r(NULL, ",", &strp);
    char* throttle = strtok_r(NULL, ",", &strp);
    char* yaw = strtok_r(NULL, ",", &strp);

    if(!drone_id || !roll || !pitch || !throttle || !yaw) return ERR_ARGS;

    float rollf = strtof(roll, NULL);
    float pitchf = strtof(pitch, NULL);
    float throttlef = strtof(throttle, NULL);
    float yawf = strtof(yaw, NULL);
    float checks[4] = { rollf, pitchf, throttlef, yawf };
    int i;

    for(i = 0; i < 4; i++)
	if(checks[i] < -1.0 || checks[i] > 1.0) return ERR_ARGS;

    queue_t* drones = process->server->air->drones;
    target_t target;
    comm_status_t s = op_comm_get_target(process, command, drone_id, 0, &
					 target);
    
    if(s != ERR_NONE) return s;

    int rolld = *((int*)&rollf);
    int pitchd = *((int*)&pitchf);
    int throttled = *((int*)&throttlef);
    int yawd = *((int*)&yawf);
        
    pthread_mutex_lock(&(drones->lock));
    drone_target_send(&target, "PCMD", "%d,%d,%d,%d,%d", 1, rolld, pitchd,
		      throttled, yawd);
    pthread_mutex_unlock(&(drones->lock));

    op_client_send(command->client, "+ PILOT (%s)", drone_id);
    return ERR_NONE;
}

/** ------------------------------------------------------------------------ **
 ** COMMAND HANDLER: STATUS REPORT
 ** ------------------------------------------------------------------------ **/

comm_status_t op_comm_status(op_command_t* command,
			     op_comm_process_t* process){
    if(!command->args || strlen(command->args) < 1)
	return ERR_ARGS;

    queue_t* drones = process->server->air->drones;
    drone_t* drone = NULL;
    comm_status_t s = op_comm_get_drone(process, command, command->args, 0,
					&drone);

    if(s != ERR_NONE) return s;

    int state = drone->state & AR_STMASK_FLY != 0 ? NVST_FLYING : NVST_LANDED;
    op_client_send(command->client, "+ STATUS (%d)", state);
    return ERR_NONE;
}
