/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#include "feedback.h"

feedback_server_t* feedback_init(airspace_t* air){
    if(!air) return NULL;

    int navdata_sock;

    if((navdata_sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	return NULL;

    if(fcntl(navdata_sock, F_SETFL,
	     fcntl(navdata_sock, F_GETFL) | O_NONBLOCK) < 0)
	goto cleanup;

    struct sockaddr_in navdata_addr;
    navdata_addr.sin_family = AF_INET;
    navdata_addr.sin_port = htons(AR_NAVDATA_PORT);
    navdata_addr.sin_addr.s_addr = air->master_addr.sin_addr.s_addr;

    if(bind(navdata_sock, (struct sockaddr*)&navdata_addr,
	    sizeof(navdata_addr)) < 0)
	goto cleanup;

    struct ip_mreq ireq = {0};
    ireq.imr_multiaddr.s_addr = inet_addr(AR_NAVDATA_MCAST);
    ireq.imr_interface.s_addr = air->master_addr.sin_addr.s_addr;

    if(setsockopt(navdata_sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &ireq,
		  sizeof(ireq)) < 0)
	goto cleanup;

    log_entry(LOG_FEEDBACK, "Feedback server initialised on port %d",
	      AR_NAVDATA_PORT);
    
    feedback_server_t* server = malloc(sizeof(feedback_server_t));
    server->active = 0;
    server->air = air;
    server->navdata_sock = navdata_sock;
    
    return server;

cleanup:
    close(navdata_sock);
    return NULL;
}

void feedback_free(feedback_server_t* server){
    if(!server) return;
    close(server->navdata_sock);
    free(server);
}

void* feedback_worker(void* raw_feedback){
    feedback_server_t* server = (feedback_server_t*)raw_feedback;
    if(!server) pthread_exit(NULL);

    fd_set readfds;
    iterator_t it;
    drone_t *drone;
    char readtest;
    size_t n, ntmp;
    struct timeval timeout;
    struct sockaddr_in wakeup;
    
    server->active = 1;
    while(server->active){
	FD_ZERO(&readfds);
	FD_SET(server->navdata_sock, &readfds);
	
	timeout.tv_sec = WATCH_TIMEOUT;
	timeout.tv_usec = 0;

	pthread_mutex_lock(&(server->air->drones->lock));
	it = queue_iterator(server->air->drones);
	for(; IT_HASNEXT(it); IT_NEXT(it)){
	    drone = IT_CUR(it, drone_t);
	    if(drone->navdata_awake) drone_navdata_kick(server, drone);
	    else drone_navdata_wakeup(server, drone);
	}
	pthread_mutex_unlock(&(server->air->drones->lock));
	
	if(select(FD_SETSIZE, &readfds, NULL, NULL, &timeout) <= 0)
	    continue;
	else if(FD_ISSET(server->navdata_sock, &readfds))
	    feedback_navdata(server);
    }

    log_entry(LOG_FEEDBACK, "Exiting now");    
    pthread_exit(NULL);
}

void feedback_navdata(feedback_server_t* server){
    if(!server) return;

    int s = server->navdata_sock;
    struct sockaddr_in addr;
    socklen_t addrlen = sizeof(addr);
    
    char buf[AR_NAVDATA_BUFSIZE];
    size_t n = recvfrom(s, buf, sizeof(buf), 0, (struct sockaddr*)&addr,
			&addrlen);
    int cur = 0;

    drone_t* drone = NULL;
    iterator_t it = queue_iterator(server->air->drones);
    for(; !drone && IT_HASNEXT(it); IT_NEXT(it)){
	drone = IT_CUR(it, drone_t);
	if(drone->addr.sin_addr.s_addr != addr.sin_addr.s_addr)
	    drone = NULL;
    }

    if(!drone) return;

    uint32_t stub;
    navdata_hdr_t hdr = {0};
    navdata_opt_t opt = {0};
    char* data;
    size_t left;
    
    while(cur < n){
	memcpy(&stub, &buf[cur], sizeof(stub));
	cur += sizeof(stub);
	
	if(stub == AR_NAVDATA_HDR || stub == AR_NAVDATA_HDR + 1){
	    hdr.header = stub;
	    data = (void*)&hdr + sizeof(stub);
	    left = sizeof(hdr) - sizeof(stub);
	    
	    memcpy(data, &buf[cur], left);
	    drone->state = hdr.state;
	    cur += left;
	}
	else{
	    opt.id = stub >> 16;
	    opt.size = (stub << 16) >> 16;
	    cur += opt.size;
	}
    }
}

void drone_navdata_kick(feedback_server_t* server, drone_t* drone){
    if(!server || !drone) return;

    struct sockaddr_in navdata_addr;
    memcpy(&navdata_addr, &(drone->addr), sizeof(navdata_addr));
    navdata_addr.sin_family = AF_INET;
    navdata_addr.sin_port = htons(AR_NAVDATA_PORT);

    sendto(server->navdata_sock, "\x01\x00\x00\x00", 4, 0,
	   (struct sockaddr*)&navdata_addr, sizeof(navdata_addr));
}

void drone_navdata_wakeup(feedback_server_t* server, drone_t* drone){
    if(!server || !drone) return;

    log_entry(LOG_AIRSPACE, "Waking up navdata stream for drone %d (%s)",
	      drone->id, inet_ntoa(drone->addr.sin_addr));

    drone_navdata_kick(server, drone);
    
    target_t target;
    target.type = DRONE;
    target.target = drone;

    drone_target_send(&target, "CONFIG_IDS", "\"%s\",\"%s\",\"%s\"",
		      AR_SESSION_ID, AR_PROFILE_ID, AR_APP_ID);
    drone_target_send(&target, "CONFIG", "%s,%s", "\"general:navdata_demo\"",
		      "\"FALSE\"");
    drone_target_send(&target, "CTRL", "0");
    drone->navdata_awake = 1;
}
