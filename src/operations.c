/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#include "operations.h"

op_client_t* op_client_new(int sock, struct sockaddr_in* addr){
    op_client_t* client = malloc(sizeof(op_client_t));
    memcpy(&(client->addr), addr, sizeof(client->addr));
    memcpy(&(client->video_addr), addr, sizeof(client->addr));
    client->addr.sin_family = client->video_addr.sin_family = AF_INET;
    client->sock = sock;
    return client;
}

size_t op_client_send(op_client_t* client, char* fmt, ...){
    va_list ap;
    va_start(ap, fmt);
    char buf[RESULT_MAXLEN];
    vsprintf(buf, fmt, ap);
    va_end(ap);
    strcat(buf, "\n\0");

    return send(client->sock, buf, strlen(buf), 0);
}

void op_client_free(void* raw_client){
    op_client_t* client = (op_client_t*)raw_client;
    if(!client) return;

    close(client->sock);
    free(client);
}

op_server_t* op_server_init(airspace_t* air, int port){
    if(port < 1 || port > 65535) return NULL;

    int server_sock, yes = 1;

    /* Basic non-blocking socket setup. */
    if((server_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	return NULL;

    if(fcntl(server_sock, F_SETFL,
	     fcntl(server_sock, F_GETFL) | O_NONBLOCK) < 0)
	return NULL;

    if(setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0)
	return NULL;

    /* Bind the server socket and listen. */
    struct sockaddr_in server_addr;
    memcpy(&server_addr, &(air->master_addr),sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);

    if(bind(server_sock, (struct sockaddr*)&server_addr,
	    sizeof(server_addr)) < 0)
	goto cleanup;

    if(listen(server_sock, CLIENT_BACKLOG) < 0)
	goto cleanup;

    log_entry(LOG_OPERATIONS, "Server initialised on port %d", port);

    op_server_t* server = malloc(sizeof(op_server_t));
    server->active = 0;
    server->air = air;
    server->sock = server_sock;
    server->clients = queue_new(); /* Clients queue. */
    server->command = op_comm_init(server);
    return server;

cleanup:
    close(server_sock);
    return NULL;
}

void op_server_free(op_server_t* server){
    if(!server) return;
    close(server->sock);
    pthread_join(server->command_thd, NULL);
    /* Disconnect and free all clients. */
    queue_free(server->clients, op_client_free);
    free(server);
}

void* op_server_worker(void* raw_server){
    op_server_t* server = (op_server_t*)raw_server;
    if(!server) pthread_exit(NULL);

    int client_sock, n;
    struct sockaddr_in client_addr;
    socklen_t addrlen = sizeof(client_addr);
    
    fd_set readfds;
    iterator_t clients;
    op_client_t *client, *new_client;
    char command[COMMAND_MAXLEN];

    server->active = 1;
    pthread_create(&(server->command_thd), NULL, op_comm_worker, server->command);
    
    while(server->active){
	FD_ZERO(&readfds);

	/* Initialise the fd_set with the server and all clients. */
	FD_SET(server->sock, &readfds);
	clients = queue_iterator(server->clients);
	for(; IT_HASNEXT(clients); IT_NEXT(clients))
	    FD_SET(IT_CUR(clients, op_client_t)->sock, &readfds);

	/* Wait for something... */
	if(select(FD_SETSIZE, &readfds, NULL, NULL, NULL) <= 0)
	    continue;

	if(FD_ISSET(server->sock, &readfds)){
	    /* Server activity: a new client has arrived, accepting it. */
	    while((client_sock = accept(server->sock,
					(struct sockaddr*)&client_addr,
					&addrlen)) > 0){
		new_client = op_client_new(client_sock, &client_addr);
		queue_add(server->clients, new_client);

		log_entry(LOG_OPERATIONS, "New client: %s",
			  inet_ntoa(client_addr.sin_addr));
	    }
	}
	else{
	    /* Client activity: disconnection, or someone's saying something. */
	    clients = queue_iterator(server->clients);
	    for(; IT_HASNEXT(clients); IT_NEXT(clients)){
		client = IT_CUR(clients, op_client_t);
		if(!FD_ISSET(client->sock, &readfds))
		   continue;
		
		memset(command, 0, sizeof(command));

		/* Receive the client's command. */
		n = recv(client->sock, command, sizeof(command), 0);

		if(n <= 0){
		    /* FIN packet received: client's gone. */
		    queue_remove(&clients, op_client_free);
		    airspace_release_all(server->air, client);
		    log_entry(LOG_OPERATIONS, "Client lost: %s",
			      inet_ntoa(client->addr.sin_addr));
		}
		else{
		    char* nl = strchr(command, '\n');
		    if(nl) *nl = 0;

		    op_command_queue(server->command, client, command);
		    log_entry(LOG_OPERATIONS, "Command received (%s) [%s]",
			      inet_ntoa(client->addr.sin_addr), command);
		}
	    }
	}
    }
    
    pthread_exit(NULL);
}
