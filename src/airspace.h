/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _ARSERVER_AIRSPACE_H
#define _ARSERVER_AIRSPACE_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STDARG_H
#include <stdarg.h>
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif

#ifdef HAVE_IFADDRS_H
#include <ifaddrs.h>
#endif

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif

#ifdef HAVE_ERRNO_H
#include <errno.h>
#endif

#include "logging.h"
#include "constants.h"
#include "util.h"
#include "feedback_navdata.h"

/* A few useful macros for IP manipulation/iteration. */
#define IP2L(a) (ntohl(((a).sin_addr.s_addr)))
#define IP_SET(a, b) ((a).sin_addr.s_addr = (b).sin_addr.s_addr)
#define IP_LTE(a, b) (IP2L(a) <= IP2L(b))
#define IP_INC(a) ((a).sin_addr.s_addr = htonl(ntohl((a).sin_addr.s_addr) + 1))

/* Structure representing a drone. */
typedef struct op_client op_client_t;
typedef struct drone{
    unsigned int id;
    unsigned int seq;
    
    int control_sock;
    int out_sock;
    
    struct sockaddr_in addr;
    op_client_t* handler;
    unsigned int group_refs;

    unsigned char navdata_awake;
    unsigned char parsing_navdata;
    drone_navdata_t navdata;
    
    navdata_state_t state;
    unsigned char emergency;
} drone_t;

typedef struct group{
    unsigned int id;
    op_client_t* handler;
    queue_t* drones;
} group_t;

typedef struct target{
    enum{ DRONE, GROUP } type;
    void* target;
} target_t;

/* Structure representing the airspace/network as a whole. */
typedef struct airspace{
    unsigned char active;
    struct sockaddr_in master_addr;
    struct sockaddr_in start;
    struct sockaddr_in end;

    unsigned int next_id;
    unsigned int next_gid;
    queue_t* drones;
    queue_t* groups;
} airspace_t;

/* Creates a drone. */
drone_t* drone_new(airspace_t* air, unsigned int id, int sock,
		   struct sockaddr_in* addr);
/* Drone command. */
size_t drone_target_send(target_t* target, char* at, char* argsfmt, ...);
/* Frees a drone. */
void drone_free(void* raw_drone);

/* Creates a group. */
group_t* group_new(airspace_t* air, op_client_t* handler);
/* Retrieves a group. */
group_t* group_get(airspace_t* air, unsigned int id);
/* Deletes a group. */
void group_del(airspace_t* air, unsigned int id);
/* Frees a group. */
void group_free(void* raw_group);

/* Initialises the airspace (network) on an interface. */
airspace_t* airspace_init(char* interface);
/* Frees the entire airspace (and drones). */
void airspace_free(airspace_t* air);
/* Concurrent worker: network scan. */
void* airspace_worker(void* raw_airspace);
/* Removes all client references. */
void airspace_remove_all(airspace_t* air, drone_t* drone);
/* Removes all client references. */
void airspace_release_all(airspace_t* air, op_client_t* client);

#endif
