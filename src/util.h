/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _ARSERVER_UTIL_H
#define _ARSERVER_UTIL_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

/* Min/max macros. */
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

/* Iterators-related macros. */
#define IT_CUR(it, type) ((type*)(((it).current)->data))
#define IT_HASNEXT(it) ((it).current != NULL)
#define IT_NEXT(it) ((it).current = (it).current ? ((it).current)->next : NULL)

/* Double-linked double-ended list element. */
typedef struct element{
    void* data;
    struct element *prev;
    struct element *next;
} element_t;

/* Double-linked double-ended list. */
typedef struct queue{
    pthread_mutex_t lock;
    element_t* first;
    element_t* last;
    size_t len;
} queue_t;

/* Double-linked double-ended list iterator. */
typedef struct iterator{
    queue_t* queue;
    element_t* current;
} iterator_t;

/* Creates a DLDE list. */
queue_t* queue_new();
/* Frees a DLDE list. */
void queue_free(queue_t* queue, void (*free_func)(void* data));
/* Adds an element to a DLDE list. */
void queue_add(queue_t* queue, void* data);
/* Removes (and free?) an element from a DLDE list. */
void queue_remove(iterator_t* iterator, void (*free_func)(void* data));
/* Returns an iterator over a DLDE list. */
iterator_t queue_iterator(queue_t* queue);

#endif
