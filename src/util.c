/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#include "util.h"

queue_t* queue_new(){
    queue_t* queue = malloc(sizeof(queue_t));
    queue->first = queue->last = NULL;
    queue->len = 0;
    pthread_mutex_init(&(queue->lock), NULL);
    return queue;
}

void queue_free(queue_t* queue, void (*free_func)(void* data)){
    if(!queue) return;
    pthread_mutex_lock(&(queue->lock));
    
    element_t *cur = queue->first, *tmp;
    while(cur){
	tmp = cur->next;
	if(free_func) free_func(cur->data);
	free(cur);
	cur = tmp;
    }
    
    pthread_mutex_unlock(&(queue->lock));
    free(queue);
}

void queue_add(queue_t* queue, void* data){
    if(!queue || !data) return;
    pthread_mutex_lock(&(queue->lock));

    element_t* element = malloc(sizeof(element_t));
    element->prev = element->next = NULL;
    element->data = data;

    if(queue->first == queue->last){
	if(!queue->first) queue->first = queue->last = element;
	else{
	    element->prev = queue->first;
	    queue->first->next = element;
	    queue->last = element;
	}
    }
    else{
	queue->last->next = element;
	element->prev = queue->last;
	queue->last = element;
    }

    (queue->len)++;
    pthread_mutex_unlock(&(queue->lock));
}

void queue_remove(iterator_t* iterator, void (*free_func)(void* data)){
    if(!iterator || !iterator->queue || !iterator->current ||
       !iterator->queue->first)
	return;

    queue_t* queue = iterator->queue;
    element_t* cur = iterator->current;
    iterator->current = iterator->current->prev;

    if(cur == queue->first && cur == queue->last)
	queue->first = queue->last = NULL;
    else{	
	if(cur == queue->first){
	    queue->first = cur->next;
	    cur->next->prev = NULL;
	}
	else if(cur == queue->last){
	    queue->last = cur->prev;
	    cur->prev->next = NULL;
	}
	else{
	    cur->prev->next = cur->next;
	    cur->next->prev = cur->prev;
	}
    }
        
    if(free_func) free_func(cur->data);
    free(cur);
    (queue->len)--;
}

iterator_t queue_iterator(queue_t* queue){
    iterator_t it;
    it.queue = queue;
    it.current = queue->first;
    return it;
}
