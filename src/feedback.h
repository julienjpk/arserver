/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _ARSERVER_FEEDBACK_H
#define _ARSERVER_FEEDBACK_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif

#include "airspace.h"
#include "util.h"
#include "feedback_navdata.h"

/* Structure representing the feedback server. */
typedef struct feedback_server{
    unsigned char active;
    airspace_t* air;

    int navdata_sock;
} feedback_server_t;

/* Initialises a feedback server. */
feedback_server_t* feedback_init(airspace_t* air);
/* Frees the feedback server. */
void feedback_free(feedback_server_t* server);
/* Feedback worker. */
void* feedback_worker(void* raw_feedback);
/* Feedback handler: navdata parser. */
void feedback_navdata(feedback_server_t* server);

/* Navdata wakeup functions. */
void drone_navdata_kick(feedback_server_t* server, drone_t* drone);
void drone_navdata_wakeup(feedback_server_t* server, drone_t* drone);

#endif
