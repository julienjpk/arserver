/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _ARSERVER_LOGGING_H
#define _ARSERVER_LOGGING_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_STDARG_H
#include <stdarg.h>
#endif

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

#ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#define LOG_MAXLEN 1024

#ifndef DEBUG
/* DEBUG not defined, ignoring all log_entry calls. */
#define log_entry(fac, fmt, ...) ;
#else

typedef enum log_facility {
    LOG_UNKNOWN = 0,
    LOG_AIRSPACE,
    LOG_FEEDBACK,
    LOG_OPERATIONS,
    LOG_COMMAND
} log_facility_t;

/* DEBUG defined, log_entry works. */
void log_entry(log_facility_t facility, char* fmt, ...);

#endif

#endif
