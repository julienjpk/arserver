/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _ARSERVER_CONST_H
#define _ARSERVER_CONST_H

/* AT configuration port. */
#define AR_AT_PORT 5556
/* Navdata buffer size. */
#define AR_NAVDATA_BUFSIZE 4096
/* Navdata port. */
#define AR_NAVDATA_PORT 5554
/* Navdata multicast group. */
#define AR_NAVDATA_MCAST "224.1.1.1"
/* ARDrone control port (connection attempts are made there). */
#define AR_CONTROL_PORT 5559
/* Max. AT command length. */
#define AT_MAXLEN 1024

/* ARDrone-specific IDs. */
#define AR_SESSION_ID "d2e081a3"
#define AR_PROFILE_ID "be27e2e4"
#define AR_APP_ID "d87f7e0c"
#define AR_NAVDATA_HDR 0x55667788

/* Delay between scans. */
#define WATCH_DELAY 2
/* Delay between airspace checks. */
#define WATCH_TIMEOUT 2
/* Connection timeout before a host is disregarded. */
#define CONNECT_TIMEOUT 125000

/* String used to wakeup navdata streams. */
#define NAVDATA_WAKEUP "NAVDATA"

typedef enum navdata_state{
    NVST_UNKNOWN = 0,
    NVST_INIT = 1,
    NVST_LANDED = 2,
    NVST_FLYING = 3,
    NVST_HOVERING = 4,
    NVST_TAKEOFF = 6,
    NVST_FLYING2 = 7,
    NVST_LANDING = 8
} navdata_state_t;

typedef enum comm_status{
    ERR_NONE = 0,
    ERR_SYNTAX = 1,
    ERR_UNKNOWN = 2,
    ERR_ARGS = 3,
    ERR_DENIED = 4,
    ERR_NODRONE = 5,
    ERR_BUSY = 6,
    ERR_UNAVAIL = 7,
    ERR_CONTEXT = 8,
    ERR_REPEAT = 9,
    ERR_NOEMERG = 10,
    ERR_SILENT = 11
} comm_status_t;

typedef enum comm_type{
    COMM_ACQUIRE,
    COMM_RELEASE,
    COMM_AIRSPACE,
    COMM_NEWGROUP,
    COMM_DELGROUP,
    COMM_ATTACH,
    COMM_DETACH,
    COMM_TAKEOFF,
    COMM_LANDING,
    COMM_EMERGENCY,
    COMM_ROLL,
    COMM_PITCH,
    COMM_THROTTLE,
    COMM_YAW,
    COMM_PILOT,
    COMM_STATUS,
    COMM_VIDEO
} comm_type_t;

#endif
