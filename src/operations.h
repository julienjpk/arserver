/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _ARSERVER_OPERATIONS_H
#define _ARSERVER_OPERATIONS_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STDARG_H
#include <stdarg.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif

#include "airspace.h"
#include "util.h"
#include "command.h"

#define CLIENT_BACKLOG 32
#define COMMAND_MAXLEN 1024
#define RESULT_MAXLEN 1024

/* Structure representing a client (master server). */
typedef struct op_client{
    int sock;
    struct sockaddr_in addr;
    struct sockaddr_in video_addr;
} op_client_t;

/* Master server and its clients. */
typedef struct op_server{
    unsigned char active;
    airspace_t* air;
    int sock;

    op_comm_process_t* command;
    pthread_t command_thd;
    queue_t* clients;
} op_server_t;

/* Creates a new client structure for a given socket and IP pair. */
op_client_t* op_client_new(int sock, struct sockaddr_in* addr);
/* Sends a message to a client. */
size_t op_client_send(op_client_t* client, char* fmt, ...);
/* Frees a client (used when freeing a client queue). */
void op_client_free(void* raw_client);
/* Initialises the master server on a given port. */
op_server_t* op_server_init(airspace_t* air, int port);
/* Master server workers: handles commands. */
void* op_server_worker(void* raw_server);
/* Closes and frees a master server. */
void op_server_free(op_server_t* server);

#endif
