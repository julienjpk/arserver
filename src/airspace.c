/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#include "airspace.h"
#include "feedback.h"

drone_t* drone_new(airspace_t* air, unsigned int id, int sock,
		   struct sockaddr_in *addr){
    drone_t* drone = malloc(sizeof(drone_t));
    memcpy(&(drone->addr), addr, sizeof(drone->addr));
    drone->addr.sin_family = AF_INET;
    drone->id = id;
    drone->control_sock = sock;

    struct sockaddr_in here;
    memcpy(&here, &(air->master_addr), sizeof(here));
    here.sin_family = AF_INET;
    here.sin_port = htons(AR_AT_PORT);
    
    drone->out_sock = socket(AF_INET, SOCK_DGRAM, 0);
    bind(drone->out_sock, (struct sockaddr*)&here, sizeof(here));

    drone->seq = 1;
    drone->group_refs = 0;
    drone->parsing_navdata = 0;
    drone->navdata_awake = 0;
    drone->handler = NULL;
    drone->state = NVST_UNKNOWN;
    drone->emergency = 0;
    return drone;
}

size_t drone_target_send(target_t* target, char* at, char* argsfmt, ...){
    if(!target || !at) return;

    char atbuf[AT_MAXLEN], argsbuf[AT_MAXLEN];
    struct sockaddr_in addr;
    memset(atbuf, 0, AT_MAXLEN);
    memset(argsbuf, 0, AT_MAXLEN);
    
    if(argsfmt){
	va_list ap;
	va_start(ap, argsfmt);
	vsprintf(argsbuf, argsfmt, ap);
	va_end(ap);
    }

    drone_t* drone;
    group_t* group;
    iterator_t it;
    size_t n;
    
    switch(target->type){
	case DRONE:
	    drone = (drone_t*)(target->target);
	    memcpy(&addr, &(drone->addr), sizeof(addr));
	    addr.sin_port = htons(AR_AT_PORT);
	    
	    sprintf(atbuf, "AT*%s=%d", at, (drone->seq)++);
	    if(argsfmt) sprintf(atbuf, "%s,%s", atbuf, argsbuf);

	    log_entry(LOG_AIRSPACE, "Outgoing AT command (drone %d, %s) : [%s]",
		      drone->id, inet_ntoa(drone->addr.sin_addr), atbuf);

	    strcat(atbuf, "\r");
	    return sendto(drone->out_sock, atbuf, strlen(atbuf), 0,
			  (struct sockaddr*)&addr, sizeof(addr));

	case GROUP:
	    group = (group_t*)(target->target);

	    it = queue_iterator(group->drones);
	    for(; IT_HASNEXT(it); IT_NEXT(it)){
		drone = IT_CUR(it, drone_t);
		memcpy(&addr, &(drone->addr), sizeof(addr));
		addr.sin_port = htons(AR_AT_PORT);
		
		sprintf(atbuf, "AT*%s=%d", at, (drone->seq)++);
		if(argsfmt) sprintf(atbuf, "%s,%s", atbuf, argsbuf);
		
		log_entry(LOG_AIRSPACE, "Outgoing AT group command (group %d, "
			  "drone %d) : [%s]", group->id, drone->id, atbuf);

		strcat(atbuf, "\r");
		
		n = sendto(drone->out_sock, atbuf, strlen(atbuf), 0,
			   (struct sockaddr*)&addr, sizeof(addr));
	    }

	    return n;
    }

    return -1;
}

void drone_free(void* raw_drone){
    drone_t* drone = (drone_t*)raw_drone;
    if(!drone) return;
    close(drone->out_sock);
    close(drone->control_sock);
    free(drone);
}

group_t* group_new(airspace_t* air, op_client_t* handler){
    if(!air) return NULL;
    
    group_t* group = malloc(sizeof(group_t));
    group->id = (air->next_gid)++;
    group->handler = handler;
    group->drones = queue_new();
    return group;
}

group_t* group_get(airspace_t* air, unsigned int id){
    if(!air) return NULL;
    iterator_t it = queue_iterator(air->groups);
    for(; IT_HASNEXT(it); IT_NEXT(it))
	if(IT_CUR(it, group_t)->id == id)
	    return IT_CUR(it, group_t);
    return NULL;
}

void group_del(airspace_t* air, unsigned int id){
    if(!air) return;
    iterator_t it = queue_iterator(air->groups);
    for(; IT_HASNEXT(it); IT_NEXT(it)){
	if(IT_CUR(it, group_t)->id == id){
	    queue_remove(&it, group_free);
	    return;
	}
    }
}

void group_free(void* raw_group){
    if(!raw_group) return;
    group_t* group = (group_t*)raw_group;
    iterator_t it = queue_iterator(group->drones);

    for(; IT_HASNEXT(it); IT_NEXT(it))
	(IT_CUR(it, drone_t)->group_refs)--;
    
    queue_free(group->drones, NULL);
    free(raw_group);
}

airspace_t* airspace_init(char* interface){
    struct ifaddrs* addrs = NULL, *itf = NULL;
    if(getifaddrs(&addrs) < 0) return NULL;

    /* Determine the interface we're targetting... */
    for(itf = addrs; itf; itf = itf->ifa_next){
	if(strcmp(itf->ifa_name, interface) == 0 &&
	   itf->ifa_addr->sa_family == AF_INET) break;
    }

    if(!itf) return NULL;

    /* Prepare airspace structure, and set network boundaries. */
    airspace_t* air = malloc(sizeof(airspace_t));
    memcpy(&(air->master_addr), itf->ifa_addr, sizeof(air->master_addr));
    memcpy(&(air->start), itf->ifa_addr, sizeof(air->start));
    memcpy(&(air->end), itf->ifa_broadaddr, sizeof(air->start));

    in_addr_t start = air->start.sin_addr.s_addr;
    in_addr_t end = air->end.sin_addr.s_addr;
    in_addr_t mask = ((struct sockaddr_in*)(itf->ifa_netmask))->sin_addr.s_addr;
    
    air->start.sin_addr.s_addr = htonl(ntohl(start & mask) + 1);
    air->end.sin_addr.s_addr = htonl(ntohl(end) - 1);

    log_entry(LOG_AIRSPACE, "Setup complete on %s (%s)", interface,
	      inet_ntoa(air->master_addr.sin_addr));
    
    air->drones = queue_new();
    air->groups = queue_new();
    air->active = 0;
    air->next_id = air->next_gid = 1;
    return air;
}

void airspace_free(airspace_t* air){
    if(!air) return;
    queue_free(air->drones, drone_free);
    queue_free(air->groups, group_free);
    free(air);
}

void* airspace_worker(void* raw_airspace){
    airspace_t* air = (airspace_t*)raw_airspace;
    if(!air) pthread_exit(NULL);

    struct sockaddr_in cur, peer;
    socklen_t peerlen = sizeof(peer);
    struct timeval timeout;
    fd_set writefds;
    int i, s, soerr, peersock;
    socklen_t intlen = sizeof(s);
    drone_t *drone;
    iterator_t it;
    
    air->active = 1;

    /* This worker will keep going until told otherwise. */
    while(air->active){
	log_entry(LOG_AIRSPACE, "Starting network scan now",
		  inet_ntoa(air->start.sin_addr));

	/* Prepare a network scan from the network's base IP address. */
	IP_SET(cur, air->start);
	cur.sin_family = AF_INET;
	cur.sin_port = htons(AR_CONTROL_PORT);

	/* Iterate over all possible IPs... */
	while(air->active && IP_LTE(cur, air->end)){
	    /* Attempt a non-blocking connect to the peer host... */
	    peersock = socket(AF_INET, SOCK_STREAM, 0);
	    fcntl(peersock, F_SETFL, fcntl(peersock, F_GETFL) | O_NONBLOCK);
	    s = connect(peersock, (struct sockaddr*)&cur, sizeof(cur));
	    IP_INC(cur);

	    if(s < 0 && errno != EINPROGRESS){
		/* Connection immediately refused, disregard. */
		close(peersock);
		continue;
	    }

	    /* Wait for a certain amount of time... */
	    FD_ZERO(&writefds);
	    FD_SET(peersock, &writefds);
	    timeout.tv_sec = CONNECT_TIMEOUT / 1000000;
	    timeout.tv_usec = CONNECT_TIMEOUT - 1000000 * timeout.tv_sec;

	    if(select(FD_SETSIZE, NULL, &writefds, NULL, &timeout) <= 0){
		close(peersock);
		continue;
	    }

	    /* Something happened on the descriptor, there might be a drone. */
	    else if(FD_ISSET(peersock, &writefds)){
		/* Confirming drone presence by checking the connection status 
		 * and trying to get the IP back. */
		s = getsockopt(peersock, SOL_SOCKET,
			       SO_ERROR, &soerr, &intlen);

		
		if(s != 0 || soerr != 0 || getpeername(peersock,
						       (struct sockaddr*)&peer,
						       &peerlen) != 0){
		    close(peersock);
		    continue;
		}

		drone = NULL;
		pthread_mutex_lock(&(air->drones->lock));
		it = queue_iterator(air->drones);
		for(; !drone && IT_HASNEXT(it); IT_NEXT(it)){
		    drone = IT_CUR(it, drone_t);
		    if(cur.sin_addr.s_addr != drone->addr.sin_addr.s_addr)
			drone = NULL;
		}
		pthread_mutex_unlock(&(air->drones->lock));

		if(drone){
		    log_entry(LOG_AIRSPACE, "Ignoring drone at %s (already "
			      "registered)", inet_ntoa(peer.sin_addr));
		    close(peersock);
		    continue;
		}

		drone = drone_new(air, (air->next_id)++, peersock, &peer);
		
		log_entry(LOG_AIRSPACE, "Drone found: %s, "
			  "assigned id %d", inet_ntoa(peer.sin_addr),
			  drone->id);
		
		queue_add(air->drones, drone);
	    }
	    else close(peersock);
	}

	pthread_exit(NULL);
	sleep(WATCH_DELAY);
    }

    log_entry(LOG_AIRSPACE, "Exiting now");    
    pthread_exit(NULL);
}

void airspace_remove_all(airspace_t* air, drone_t* drone){
    if(!air || !drone) return;

    iterator_t itg = queue_iterator(air->groups), itd;
    for(; IT_HASNEXT(itg); IT_NEXT(itg)){
	itd = queue_iterator(IT_CUR(itg, group_t)->drones);
	for(; IT_HASNEXT(itd); IT_NEXT(itd))
	    if(IT_CUR(itd, drone_t)->id == drone->id)
		queue_remove(&itd, NULL);
    }
}

void airspace_release_all(airspace_t* air, op_client_t* client){
    if(!air || !client) return;
    
    iterator_t it = queue_iterator(air->groups);
    group_t* group;
    
    for(; IT_HASNEXT(it); IT_NEXT(it)){
	group = IT_CUR(it, group_t);
	if(group->handler == client)
	    queue_remove(&it, group_free);
    }

    it = queue_iterator(air->drones);
    drone_t* drone;

    for(; IT_HASNEXT(it); IT_NEXT(it)){
	drone = IT_CUR(it, drone_t);
	if(drone->handler == client)
	    drone->handler = NULL;
    }
}
