/* This file is part of arserver.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 
 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
   
 * You should have received a copy of the GNU General Public License along with 
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#include "logging.h"

#ifdef DEBUG

static pthread_mutex_t out_mutex = PTHREAD_MUTEX_INITIALIZER;

static struct facility_style {
    log_facility_t facility;
    char* name;
    unsigned char colour;
} styles[] = {
    { LOG_AIRSPACE, "airspace", 36 },
    { LOG_FEEDBACK, "feedback", 32 },
    { LOG_OPERATIONS, "operations", 31 },
    { LOG_COMMAND, "command", 35 },
    { LOG_UNKNOWN, "?", 37 }
};

void log_entry(log_facility_t facility, char* fmt, ...){
    struct facility_style* style;
    for(style = styles;
	style->facility != facility && style->facility != LOG_UNKNOWN;
	style++);

    time_t time_now;
    time(&time_now);
    struct tm tm_now;
    localtime_r(&time_now, &tm_now);
    char str_now[32];
    strftime(str_now, 32, "%d %b %Y %I:%M:%S", &tm_now);

    char full_fmt[LOG_MAXLEN];
    sprintf(full_fmt, "[%s / \033[1;%dm%s\033[0m]%-*s%s.\n", str_now,
	    style->colour, style->name, 13 - (int)strlen(style->name),
	    " ", fmt);

    va_list ap;
    va_start(ap, fmt);
    pthread_mutex_lock(&out_mutex);
    vfprintf(stderr, full_fmt, ap);
    pthread_mutex_unlock(&out_mutex);
    va_end(ap);
}

#endif
