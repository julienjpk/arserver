/* This file is part of arserver.
 * Special thanks to the ARDrone autonomy project for its values.

 * arserver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * arserver is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along with
 * arserver. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _ARSERVER_NAVDATA_H
#define _ARSERVER_NAVDATA_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif

/* Navdata header. */
typedef struct navdata_hdr{
    uint32_t header;
    uint32_t state;
    uint32_t seq;
    uint32_t vision;
} navdata_hdr_t;

/* Navdata option (piece of information). */
typedef struct navdata_opt {
    uint16_t id;
    uint16_t size;
    void* data;
} navdata_opt_t;

/* Navdata checksum (unused for now). */
typedef struct navdata_cks{
    uint16_t id;
    uint16_t size;
    uint32_t data;
} navdata_cks_t;

/* Structure containing every piece of navdata for a drone. At the moment,
 * there's just a state bitmask. */
typedef struct drone_navdata{
    uint32_t state;
} drone_navdata_t;

/* State masks, thanks to ARDrone autonomy! */
typedef enum ar_stmask {
    /* FLY MASK : (0) Ardrone is landed, (1) Ardrone is flying */
    AR_STMASK_FLY                   = 1 <<  0,
    /* VIDEO MASK : (0) Video disable, (1) Video enable */
    AR_STMASK_VIDEO                 = 1 <<  1,
    /* VISION MASK : (0) Vision disable, (1) Vision enable */
    AR_STMASK_VISION                = 1 <<  2,
    /* CONTROL ALGO : (0) Euler angles control, (1) Angular speed control */
    AR_STMASK_CONTROL               = 1 <<  3,
    /* ALTITUDE CONTROL ALGO : (0) Control inactive (1) Control active */
    AR_STMASK_ALTITUDE              = 1 <<  4,
    /* USER feedback :     Start button state */
    AR_STMASK_USER_FEEDBACK_START   = 1 <<  5,
    /* Control command ACK : (0) None, (1) One received */
    AR_STMASK_COMMAND               = 1 <<  6,
    /* CAMERA MASK : (0) Camera not ready, (1) Camera ready */
    AR_STMASK_CAMERA                = 1 <<  7,
    /* Travelling mask : (0) Disable, (1) Enable */
    AR_STMASK_TRAVELLING            = 1 <<  8,
    /* USB key : (0) Usb key not ready, (1) Usb key ready */
    AR_STMASK_USB                   = 1 <<  9,
    /* Navdata demo : (0) All navdata, (1) Only navdata demo */
    AR_STMASK_NAVDATA_DEMO          = 1 << 10,
    /* Navdata bootstrap : (0) Demo or options sent in all, (1) No options */
    AR_STMASK_NAVDATA_BOOTSTRAP     = 1 << 11,
    /* Motors status : (0) Ok, (1) Motors problem */
    AR_STMASK_MOTORS                = 1 << 12,
    /* Communication Lost : (1) Com problem, (0) Com is ok */
    AR_STMASK_COM_LOST              = 1 << 13,
    /* VBat low : (1) Too low, (0) Ok */
    AR_STMASK_VBAT_LOW              = 1 << 15,
    /* User Emergency Landing : (1) User EL is ON, (0) User EL is OFF */
    AR_STMASK_USER_EL               = 1 << 16,
    /* Timer elapsed : (1) Elapsed, (0) Not elapsed */
    AR_STMASK_TIMER_ELAPSED         = 1 << 17,
    /* Angles : (0) Ok, (1) Out of range */
    AR_STMASK_ANGLES_OUT_OF_RANGE   = 1 << 19,
    /* Ultrasonic sensor : (0) Ok, (1) Deaf */
    AR_STMASK_ULTRASOUND            = 1 << 21,
    /* Cutout system detection : (0) Not detected, (1) Detected */
    AR_STMASK_CUTOUT                = 1 << 22,
    /* PIC Version number OK : (0) Bad version number, (1) OK */
    AR_STMASK_PIC_VERSION           = 1 << 23,
    /* ATCodec thread ON : (0) Thread OFF (1) thread ON */
    AR_STMASK_ATCODEC_THREAD_ON     = 1 << 24,
    /* Navdata thread ON : (0) Thread OFF (1) thread ON */
    AR_STMASK_NAVDATA_THREAD_ON     = 1 << 25,
    /* Video thread ON : (0) Thread OFF (1) thread ON */
    AR_STMASK_VIDEO_THREAD_ON       = 1 << 26,
    /* Acquisition thread ON : (0) Thread OFF (1) thread ON */
    AR_STMASK_ACQ_THREAD_ON         = 1 << 27,
    /* CTRL watchdog : (1) Delay in control execution, (0) Well scheduled */
    AR_STMASK_CTRL_WATCHDOG         = 1 << 28,
    /* ADC Watchdog : (1) Delay in uart2 dsr (> 5ms), (0) Uart2 is good */
    AR_STMASK_ADC_WATCHDOG          = 1 << 29,
    /* Communication Watchdog : (1) Com problem, (0) Com is ok */
    AR_STMASK_COM_WATCHDOG          = 1 << 30,
    /* Emergency landing : (0) No emergency, (1) Emergency */
    AR_STMASK_EMERGENCY             = 1 << 31
} ar_stmask_t;

#endif
