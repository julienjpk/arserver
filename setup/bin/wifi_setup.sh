#!/bin/sh

# Initialise the random generator
cat /data/random_mac.txt > /dev/urandom
/bin/random_mac > /data/random_mac.txt

# Basic settings.
export NETIF=ath0
export WORKAREA="/lib/firmware"
export ATH_PLATFORM="parrot-omap-sdio"
export ATH_MODULE_ARGS="ifname=$NETIF"

WIFI_MODE=`grep wifi_mode /data/config.ini | awk -F "=" '{ gsub(/ */,"",$2); print $2}'` 
case $WIFI_MODE in
    1) WIFI_MODE=ad-hoc ;;
    2) WIFI_MODE=managed ;;
    *) WIFI_MODE=master ;;
esac

# Determine MAC address.
if [ -s /factory/mac_address.txt ]; then
    MAC_ADDR=`cat /factory/mac_address.txt`
else
    MAC_ADDR=`cat /data/random_mac.txt`
fi

# Prepare the WiFi chip and wait a bit.
loadAR6000.sh -i $NETIF --setmac $MAC_ADDR
sleep 5

AR6K_PID=`ps_procps -A -T -c | grep AR6K | awk '{print $1}'`
SDIO_PID=`ps_procps -A -T -c | grep ksdioirqd | awk '{print $1}'`

# Changing wifi priority
chrt -p -r 25 $SDIO_PID
chrt -p -r 24 $AR6K_PID

# Disabling powersaving
wmiconfig -i $NETIF --power maxperf
# Disabling 802.11n aggregation
wmiconfig -i $NETIF --allow_aggr 0 0
# Enabling WMM
wmiconfig -i $NETIF --setwmm 1

# Get network SSID.
SSID=`grep ssid_single_player /data/config.ini | awk -F "=" '{gsub(/ /, "", $2); print $2}'`
if [ -z "$SSID" ]; then
    # Default SSID.
    SSID=ardrone2_wifi
fi

# Interface setup.
ifconfig $NETIF up
iwconfig $NETIF mode $WIFI_MODE
iwconfig $NETIF essid "$SSID"

if [ "$WIFI_MODE" != "managed" ]; then
    # Restrict ACS to channels 1 & 6
    wmiconfig -i $NETIF --acsdisablehichannels 1
    iwconfig $NETIF channel auto
    iwconfig $NETIF rate auto
    iwconfig $NETIF commit
else
    # The Wifi connection freezes when in managed mode if there is no keepalive
    wmiconfig -i ath0 --setkeepalive 1
fi

# Set DTIM timeout and commit changes.
wmiconfig -i $NETIF --dtim 1
wmiconfig -i $NETIF --commit
sleep 2

if [ "$WIFI_MODE" != "managed" ]; then
    # -------------------------------
    # WIRELESS SETUP
    # MASTER AND AD-HOC CONFIGURATION
    # -------------------------------
    
    OK=0
    BASE_ADRESS=192.168.1.
    PROBE=1
    
    while [ $OK -eq 0 ]; do
	# Try to use this address...
	ifconfig $NETIF $BASE_ADRESS$PROBE
	arping -I $NETIF -q -f -D -w 2 $BASE_ADRESS$PROBE
	
	if [ $? -eq 1 ]; then
	    # Already in use, let's find another.
	    if [ -s /data/old_adress.txt ]
	    then
		# Testing previously given adress.
		PROBE=`cat /data/old_adress.txt`
	    else
		# Generating random odd IP address
		PROBE=`/bin/random_ip`
	    fi
	    /bin/random_ip > /data/old_adress.txt
	else
	    # We're good.
	    echo $PROBE > /data/old_adress.txt
	    OK=1
	fi
    done
    
    # Configuring DHCP server.
    echo "Using address $BASE_ADRESS$PROBE"
    echo "start $BASE_ADRESS`expr $PROBE + 1`" > /tmp/udhcpd.conf
    echo "end $BASE_ADRESS`expr $PROBE + 4`" >> /tmp/udhcpd.conf
    echo "interface $NETIF" >> /tmp/udhcpd.conf
    echo "decline_time 1" >> /tmp/udhcpd.conf
    echo "conflict_time 1" >> /tmp/udhcpd.conf
    echo "opt router  $BASE_ADRESS$PROBE" >> /tmp/udhcpd.conf
    echo "opt subnet 255.255.255.0" >> /tmp/udhcpd.conf
    echo "opt lease  1200" >> /tmp/udhcpd.conf

    # Start the DHCP server.
    udhcpd /tmp/udhcpd.conf
else
    # -------------------------------
    # WIRELESS SETUP
    # MANAGED CONFIGURATION
    # -------------------------------

    if [ -f /etc/wep_key.conf ]; then
	iwconfig $NETIF key s:"$(head -n1 /etc/wep_key.conf)"
    fi

    # Start the DHCP client. The router will assign an IP.
    if ! udhcpc -i $NETIF -n; then
	# Lease couldn't be obtained. The drone will not be reachable over the
	# network, which is a problem. Reset configuration and reboot into
	# master mode under 60s.
	sed 's/ssid_single_player.*/ssid_single_player             = ardrone2_wifi/' -i /data/config.ini
	sed 's/ssid_multi_player.*/ssid_multi_player             = ardrone2_wifi/' -i /data/config.ini
	sed 's/wifi_mode.*/wifi_mode                      = 0/' -i /data/config.ini
	sed 's/owner_mac.*/owner_mac                      = 00:00:00:00:00:00/' -i /data/config.ini
	(sleep 60 && /sbin/reboot &)
    fi
fi

# MAC filtering (owner_mac).
/bin/pairing_setup.sh

# Saving random info for initialization at next reboot.
echo $MAC_ADDR `date` `/bin/random_mac` > /dev/urandom
/bin/random_mac > /data/random_mac.txt

# Start the telnet daemon.
telnetd -l /bin/sh

# Adding route for multicast-packet
route add -net 224.0.0.0 netmask 240.0.0.0 dev $NETIF

# Allow reconnection to dirty TCP ports
echo "1" > /proc/sys/net/ipv4/tcp_tw_reuse 
echo "1" > /proc/sys/net/ipv4/tcp_tw_recycle 

# Misc. netfilter configuration.
sysctl -w net.netfilter.nf_conntrack_tcp_timeout_syn_sent=12
sysctl -w net.netfilter.nf_conntrack_tcp_timeout_syn_recv=6
sysctl -w net.netfilter.nf_conntrack_tcp_timeout_fin_wait=12
sysctl -w net.netfilter.nf_conntrack_tcp_timeout_close_wait=6
sysctl -w net.netfilter.nf_conntrack_tcp_timeout_last_ack=10
sysctl -w net.netfilter.nf_conntrack_tcp_timeout_time_wait=12
sysctl -w net.netfilter.nf_conntrack_tcp_timeout_close=10

# Starting the daemon which responds to the AUTH messages from FreeFlight
/bin/parrotauthdaemon
